from sys import argv

# print student class performance for date 27.03.19

filepath = "data.txt"
with open(filepath) as fp:
    lines = fp.readlines()

print("Total count #%d" % len(lines))


sortedls = []
for line in lines:
    name = line.split()[0]
    score = 0
    score += line.count('++') *   8
    score += line.count('+') *    2
    score += line.count('-') *   -2
    score += line.count('+/-') * -1
    sortedls.append((name, score))

sortedls.sort(key=lambda rang: rang[1], reverse=True)
for entry in sortedls:
    print("%s: %d" % entry)
    

